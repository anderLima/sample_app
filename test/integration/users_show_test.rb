require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest

  def setup
    @user         = users(:archer)
    @user_disable = users(:beatriz)
  end

  test "should not show user disable" do
    log_in_as (@user)
    get user_path(@user_disable)
    assert_redirected_to root_url
  end

end